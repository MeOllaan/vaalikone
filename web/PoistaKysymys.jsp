<%-- 
    Document   : PoistaKysymys
    Created on : May 6, 2016, 1:30:01 PM
    Author     : juho1211
--%>

<%@page import="javax.persistence.Query"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="persist.Kysymykset2"%>
<%@page import="KysymystenHallinta.PoistaKysymysServlet"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Me Ollaan Vaalikone 2.0. Kysymysten poistaminen</title>
        <link href="style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <img id="headerimg" src="images/Logo.png" width="500" height="144" alt=""/>
        <div id="container">


            <% if (session.getAttribute("Admin") == null) { %>

            <h1>Käyttö estetty!!</h1>



            <%} else { %>
            <form action="AdminTools.jsp" method="post">
                <input type="submit" value="Takaisin" />
            </form>


            <h1>ValitsePoistettava kysymys</h1>
            <%

                EntityManagerFactory emf = (EntityManagerFactory) getServletContext().getAttribute("emf");
                EntityManager em = emf.createEntityManager();

                Query kaikkikysymykset = em.createNamedQuery("Kysymykset2.findAll");

                List<Kysymykset2> kysymysList = kaikkikysymykset.getResultList();
            %>

            <script type="text/javascript">
                function confirm_decision(valittuID) {
                    if (confirm("poistetaanko kysymys " + valittuID + "?"))
                    {
                        window.location = "http://localhost:8080/vaalikone/PoistaKysymysServlet?ID=" + valittuID;
                    } else {
                        return false;
                    }
                    return true;
                }
            </script>

            <%
                out.println("<br/>");

                for (int i = 0; i < kysymysList.size(); i++) {

                    int valittuID = kysymysList.get(i).getKysymysId();
            %>

            <input type="button" value="delete" onclick="confirm_decision('<%=valittuID%>');"/>


            <%
                        if (valittuID < 10) {
                            out.println("&nbsp;" + kysymysList.get(i).getKysymysId() + "&nbsp;&nbsp;| ");

                            out.println(kysymysList.get(i).getKysymys() + "<br/>");
                        } else {
                            out.println(kysymysList.get(i).getKysymysId() + " | ");
                            out.println(kysymysList.get(i).getKysymys() + "<br/>");
                        }
                    }

                }
            %>

        </div>
    </body>
</html>
