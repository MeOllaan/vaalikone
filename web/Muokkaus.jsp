<%-- 
    Document   : Muokkaus
    Created on : May 3, 2016, 10:32:12 PM
    Author     : konsta1401
--%>

<%@page import="persist.Kysymykset2"%>
<%@page import="java.util.List"%>
<%@page import="javax.persistence.Query"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5 Transitional//EN"                                                    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="style.css" rel="stylesheet" type="text/css">
        <title>Me Ollaan Vaalikone 2.0. Kysymysten muokkaus</title>
    </head>
    <body>
        <img id="headerimg" src="images/Logo.png" width="500" height="144" alt=""/>
        <div id="container">

            <% if (session.getAttribute("Admin") == null) { %>

            <h1>Käyttö estetty!!</h1>


            <% } else { %>


            <%
                EntityManagerFactory emf = (EntityManagerFactory) getServletContext().getAttribute("emf");
                EntityManager em = emf.createEntityManager();

                Query kaikkikysymykset = em.createNamedQuery("Kysymykset2.findAll");
                List<Kysymykset2> kysymysList = kaikkikysymykset.getResultList();
            %>

            <form action="AdminTools.jsp" method="post">
                <input type="submit" value="Takaisin" />
            </form>

            <center><TABLE border="1" >
                    <TR>
                        <TH>KYSYMYS_ID</TH>
                        <TH>KYSYMYS</TH>
                    </TR>

                    <%for (int i = 0; i < kysymysList.size(); i++) {%>
                    <tr>
                        <th>
                            <%= kysymysList.get(i).getKysymysId()%>.
                        </th>
                        <th>
                            <%= kysymysList.get(i).getKysymys()%><br/>
                        </th>


                    </tr>
                    <% } %>
                </table>
                <form name="kyssari" action="Uppi" method="post">
                    <center>Kysymyksen ID:</center>
                    <center><input name="id" type="number"  size="14" /></center>
                    <center>Uusi kysymys:</center>
                    <center><input name="kysymys" type="kysymys" size="40" /></center>
                    <center><input type="submit" name="submit" value="Muokkaa" /></center>
                </form>
                <% }%>
        </div>
    </body>
</html> 
