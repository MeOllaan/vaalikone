<%-- 
    Document   : tallennaVastaus
    Created on : May 10, 2016, 11:25:03 AM
    Author     : juho1211
--%>

<%@page import="persist.Kysymykset2"%>
<%@page import="java.util.ArrayList"%>
<%@page import="persist.Kysymykset"%>
<%@page import="java.util.List"%>
<%@page import="javax.persistence.Query"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Me Ollaan Vaalikone 2.0. Vastaa kysymyksiin</title>
        <link href="style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <img id="headerimg" src="images/Logo.png" width="500" height="144" alt=""/>
        <div id="container">
            
            <% if (session.getAttribute("Ehdokas") == null) { %>

            <h1>Käyttö estetty!!</h1>
           

            <% } else { %>
            
            <form action="AdminTools.jsp" method="post">
                <input type="submit" value="Takaisin" />
            </form>
         
        <%
            //luetaan tiedot tietokannasta
                     EntityManagerFactory emf
                            = (EntityManagerFactory) getServletContext().getAttribute("emf");
                    EntityManager em = emf.createEntityManager();

                     Query kaikkikysymykset = em.createNamedQuery("Kysymykset2.findAll");
                     

                     List<Kysymykset2> kysymysList = kaikkikysymykset.getResultList();
                     %>
        
                     
                     
        <form action="VastaustenTallennusServlet" method="POST">        
           
                <% 
                  
                     List Vastaukset = new ArrayList();
                     
                for(int i = 0; i < kysymysList.size(); i++) {
                   int ValittuKysymysID;
                   ValittuKysymysID = kysymysList.get(i).getKysymysId();
                   String Kysymys;
                   Kysymys = kysymysList.get(i).getKysymys();
                   
                   out.println(ValittuKysymysID+" | " + Kysymys);
                 %>
                 
                 <br>
                 
            erimieltä
            <input type="radio" name=<%=ValittuKysymysID%> value="1" checked> 1
            <input type="radio" name=<%=ValittuKysymysID%> value="2"> 2
            <input type="radio" name=<%=ValittuKysymysID%> value="3"> 3
            <input type="radio" name=<%=ValittuKysymysID%> value="4"> 4
            <input type="radio" name=<%=ValittuKysymysID%> value="5"> 5
            samaamieltä
                <br>
            Kommentti:
            <br>
            <textarea type="text" rows="5" cols="50" name="k<%=ValittuKysymysID%>">Kirjoita kommentti...</textarea>
                <br>
                <br>
              
             <% 
                }
              %>
              
            <input type="submit" value="Submit">
            
            
        </form>
            <% } %>  
        </div>
    </body>
</html>
