<%-- 
    Document   : AdminTools
    Created on : May 12, 2016, 4:01:49 PM
    Author     : janina1403
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="style.css" rel="stylesheet" type="text/css">
        <title>Me Ollaan Vaalikone 2.0 AdminTools</title>
    </head>

    <body>
        <img id="headerimg" src="Logo.png" width="720" />
        <div id="container">
            <%-- Sessionit on asetettu Kirjautuminen.java ja tässä tarkistetaan vain ettei ehdokas pääse admin puolelle.--%>
            <% if (session.getAttribute("Admin") == null || session.getAttribute("Ehdokas") != null) { %>
    
            <h1>Käyttö estetty!!</h1>
        
            
             <% } else { %>
             
             
            
            
            
            <form action="lisaaKysymys.jsp">
                <input id="submitnappi" type="submit" value="Lisää" />
            </form>

            <form action="Muokkaus.jsp">
                <input id="submitnappi" type="submit" value="Muokkaa" />
            </form>

            <form action="PoistaKysymys.jsp">
                <input id="submitnappi" type="submit" value="Poista" />
            </form>
            
            <form action="LisaaAdmin.jsp">
                <input id="submitnappi3" type="submit" value="Lisää Admin tai Ehdokas" />
            </form>
            
            <form action="AdminTools" method="POST">
                <input id="submitnappi" type="submit" value="Kirjaudu ulos" name="kirjauduUlos" />
            </form>
            
            
           <% }  %>
            
            
        </div>
        
        
    </body>
</html>
