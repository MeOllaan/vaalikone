<%-- 
    Document   : ehdokkaanVastaus
    Created on : May 11, 2016, 10:18:43 AM
    Author     : janina1403
--%>


<%@page import="java.util.*,vaalikone.Vaalikone,persist.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Me Ollaan Vaalikone 2.0</title>
        <link href="style.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <img id="headerimg" src="images/Logo.png" width="500" height="144" alt=""/>
        <div id="container">



            <h1>Vaalikone</h1>



            <div class="kysymys">
                <%=request.getAttribute("kysymysID")%> <br>
                <%= request.getAttribute("kysymys")%>
            </div>
            <form action="ehdokkaanVastausServlet" id="vastausformi" method="POST">
                <label>1</label><input type="radio" name="vastaus" value="1" />
                <label>2</label><input type="radio" name="vastaus" value="2" />
                <label>3</label><input type="radio" name="vastaus" value="3" checked="checked" />
                <label>4</label><input type="radio" name="vastaus" value="4" />
                <label>5</label><input type="radio" name="vastaus" value="5" />
                <input type="hidden" name="q" value="<%=request.getAttribute("kysymysID")%>">
                <p><textarea rows="6" cols="50" name="kommentti"> Kirjoita kommentti tähän... </textarea></p>



                <input type="submit" id="submitnappi" value="Vastaa" />
            </form>

            <div class="kysymys"><small>1=Täysin eri mieltä 2=Osittain eri mieltä 3=En osaa sanoa, 4=Osittain samaa mieltä 5=Täysin samaa mieltä</small></div>
        </div> 






    </body>
</html>
