
<%@page import="javax.persistence.Query"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="java.util.*,vaalikone.Vaalikone,persist.*"%>
<%@page import="persist.Vastaukset"%>
<%-- 
    Document   : ehdokas
    Created on : May 10, 2016, 1:25:11 PM
    Author     : janina1403
    
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Me Ollaan Vaalikone 2.0 Ehdokas</title>
        <link href="style.css" rel="stylesheet" type="text/css">

    </head>
    <body>
        <img id="headerimg" src="images/Logo.png" width="500" height="144" alt=""/>
        <div id="container">
            

            <% if (session.getAttribute("Ehdokas") == null) { %>

            <h1>Käyttö estetty!!</h1>
           

            <% } else { %>


            <h2>Ehdokaspaneeli</h2>

            <%
                
                /* Haetaan käyttäjätunnus sessiosta */
                String kayttajatunnus = session.getAttribute("Ehdokas").toString();
                
                /* Luodaan entiteettimanageri*/
                EntityManagerFactory emf = (EntityManagerFactory) getServletContext().getAttribute("emf");
                EntityManager em = emf.createEntityManager();
                
                try {
                
                /* Haetaan käyttäjätunnuksen perusteella ehdokas tietokannasta */
                Query haeEhdokas = em.createNamedQuery("Ehdokkaat3.findByKayttajatunnus").setParameter("kayttajatunnus", kayttajatunnus);
                
                /* Luodaan ehdokkaan tiedoista lista */
                List<Ehdokkaat3> ehdokas = haeEhdokas.getResultList();
                
                /* Haetaan listasta ehdokkaan tiedot */
                int ehdokas_id = ehdokas.get(0).getEhdokasId();
                String puolue = ehdokas.get(0).getPuolue();
                String etunimi = ehdokas.get(0).getEtunimi();
                String sukunimi = ehdokas.get(0).getSukunimi();
                int ika = ehdokas.get(0).getIka();
                String kotipaikka = ehdokas.get(0).getKotipaikkakunta();
                String ammatti = ehdokas.get(0).getAmmatti();
                String miksiEduskuntaan = ehdokas.get(0).getMiksiEduskuntaan();
                String edistys = ehdokas.get(0).getMitaAsioitaHaluatEdistaa();


            %>

            <div id="ehdokastiedot">

                <ul>

                    <li><b>Numero: </b><%= ehdokas_id%></li> 
                    <li><b>Nimi: </b><%= etunimi%> <%= sukunimi%></li>
                    <li><b>Ikä: </b><%= ika%></li>
                    <li><b>Kotipaikkakunta: </b><%= kotipaikka%></li>
                    <li><b>Ammatti: </b><%= ammatti%></li>
                    <li><b>Puolue: </b><%= puolue%></li>

                </ul></div>
            <form action="ehdokasPaneeliServlet" method="POST"> 

                <h3>Miksi haluat eduskuntaan?</h3>
                <p><textarea rows="10" cols="50" name="miksieduskuntaan"> <%= miksiEduskuntaan%> </textarea> </p>
                <h3>Mitä asioita haluat edistää?</h3>
                <p><textarea rows="10" cols="50" name="edistys"><%= edistys%></textarea></p>
                <input type="hidden" name="ehdokasid" value="<%= ehdokas_id%>">
                <p>Jos haluat muokata vastauksiasi, muokkaa ja paina tallenna vastaukset. </p>
                <p><input type="submit" value="Tallenna vastaukset"></p>


            </form>
            
            <hr>
            <h2>Kysymykset</h2>

            <%

                    /* Haetaan kaikki kysymykset tietokannasta */
                    Query kysymykset = em.createNamedQuery("Kysymykset2.findAll");
                    List<Kysymykset2> kaikkiKysymykset = kysymykset.getResultList();
                    
                    /* Listataan kaikki kysymykset ja haetaan vastaukset niihin */ 
                    for (int i = 0; i < kaikkiKysymykset.size(); i++) {%>


            <p><b><%= i + 1%>.
                    <%= kaikkiKysymykset.get(i).getKysymys()%><br/></b></p>

            <% 
                /* Luodaan primary key-entiteetti ja määritetään sille ehdokasID ja kysymysID */
                VastauksetPK vastausPK = new VastauksetPK();

                vastausPK.setEhdokasId(ehdokas_id);
                vastausPK.setKysymysId(kaikkiKysymykset.get(i).getKysymysId());
                
                /* Haetaan primary keytä vastaava vastaus tietokannasta */
                Vastaukset vastaus = em.find(Vastaukset.class, vastausPK); %>
            
            <!--- Jos primary keytä vastaavaa vastausta ei löydy tietokannasta, ilmoitetaan
                  ettei kysymykseen ole vastattu -->
            <% if (vastaus == null) { %>

            <p>Ei vastattu kysymykseen.</p>



            <% } else {%> 

            <p><% int vastausNumero = vastaus.getVastaus();
                if (vastausNumero == 1) { %>
                Täysin eri mieltä.
                <% } else if (vastausNumero == 2) { %>
                Osittain eri mieltä.
                <% } else if (vastausNumero == 3) { %>
                En osaa sanoa.
                <% } else if (vastausNumero == 4) { %>
                Osittain samaa mieltä.
                <% } else if (vastausNumero == 5) {%>
                Täysin samaa mieltä.
                <% }%>
                <br/>
                <%= vastaus.getKommentti()%></p>


            <%
                        }
                    }
                } finally {
                    em.close();
                }

            %>





            <form action="tallennaVastaus.jsp" method="POST">
                <input id="submitnappi3" type="submit" value="Vastaa vaalikysymyksiin" name="btnVastaa"/>
            </form>

            <form action="AdminTools" method="POST">
                <input id="submitnappi3" type="submit" value="Kirjaudu ulos"/>
            </form>


            <% }%>

        </div>
    </body>
</html>
