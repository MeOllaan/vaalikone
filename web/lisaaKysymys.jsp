<%@page import="javax.persistence.Query"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="java.util.*,vaalikone.Vaalikone,persist.*"%>

<%-- 
    Document   : lisaaKysymys
    Created on : May 2, 2016, 10:03:13 AM
    Author     : janina1403
--%>

<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!doctype html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Me Ollaan Vaalikone 2.0</title>

        <link href="style.css" rel="stylesheet" type="text/css">


        <!-- Scripti kysymyksen lisäämisen ilmoittamiseksi -->
        <script type="text/javascript">

            function kysymysLisattyPopUp() {
                alert("Kysymys lisätty!");
            }


        </script>

        <!-- Jos servletiltä saadaan kysymysLisatty-muuttujan arvo, näytetään pop up  -->
        <%

            if (request.getAttribute("kysymysLisatty") == null) {

            } else {%>

        <script>

            kysymysLisattyPopUp();

        </script>

        <%}%>
    </head>

    <body>
        <img id="headerimg" src="images/Logo.png" width="500" height="144" alt=""/>
        <div id="container">

            <%

                if (session.getAttribute("Admin") == null) {%>

            <h1> Käyttö estetty!!</h1>

            <% } else {%>
            
            <form action="AdminTools.jsp" method="post">
                <input type="submit" value="Takaisin" />
            </form>

            <div id="kysymysformi">
                <h1>Lisää uusi kysymys</h1>
                <form action="LisaaKysymysServlet" method="POST" name="kysymysformi">

                    <input type="text" size="50" name="kysymys"><br />
                    <input type="submit" value="Lisää kysymys"><br />

                </form>
            </div>

            <div id="kysymyslistaus">
                <h2>Kysymykset</h2>

                <%
                    /* Luodaan uusi entiteettimanageri */
                    EntityManagerFactory emf = (EntityManagerFactory) getServletContext().getAttribute("emf");
                    EntityManager em = emf.createEntityManager();
                    try {
                        
                        /* Haetaan tietokannasta kaikki kysymykset listaan */
                        Query kaikkikysymykset = em.createNamedQuery("Kysymykset2.findAll");
                        List<Kysymykset2> kysymysLista = kaikkikysymykset.getResultList();
                        
                        /* Listataan kaikki kysymykset */
                        for (int i = 0; i < kysymysLista.size(); i++) {%>

                <%= i + 1%>.
                <%= kysymysLista.get(i).getKysymys()%><br/>



                <%}
                    } finally {
                        em.close();

                    }%>
            </div>  
            <% }%>


        </div>


    </body>

</html>
