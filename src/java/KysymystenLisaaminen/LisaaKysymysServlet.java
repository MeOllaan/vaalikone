/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KysymystenLisaaminen;

import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import persist.Kysymykset2;

/**
 *
 * @author janina1403
 */
public class LisaaKysymysServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        if (session.getAttribute("Admin") == null) {

            out.println("<h1>Käyttö estetty!!</h1>");

        } else {

            //Määritetään parametrina haettu kysymys
            String kysymysTeksti = request.getParameter("kysymys");

            //kysymysLisatty-muuttujaa käytetään lisaaKysymys.jsp:n pop upissa
            String kysymysLisatty;

            //Luodaan entitymanageri
            EntityManagerFactory emf
                    = (EntityManagerFactory) getServletContext().getAttribute("emf");
            EntityManager em = emf.createEntityManager();

            try {

                //luodaan uusi entiteetti ja määritetään kysymysteksti. Kysymyksen ID generoidaan
                //automaattisesti.
                Kysymykset2 kysymys = new Kysymykset2(kysymysTeksti);

                //lisätään tiedot tietokantaan
                em.getTransaction().begin();
                em.persist(kysymys);
                em.getTransaction().commit();

                //Lähetetään jsplle string "lisatty", jotta voidaan näyttää pop up
                kysymysLisatty = "lisatty";
                request.setAttribute("kysymysLisatty", kysymysLisatty);

            } finally {

                //suljetaan entitymanager
                em.close();

                //Ohjataan takaisin lisaaKysymys.jsp:lle
                request.getRequestDispatcher("/lisaaKysymys.jsp")
                        .forward(request, response);

            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
