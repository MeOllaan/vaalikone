/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Update;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import persist.Kysymykset2;

/**
 *
 * @author konsta1401
 */
public class Muokkaus extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            
            HttpSession session = request.getSession();
        
        if (session.getAttribute("Admin") == null) {
            
        out.println("<h1>Käyttö estetty!!</h1>");
            
        } else {
        
            
            EntityManagerFactory emf = (EntityManagerFactory) getServletContext().getAttribute("emf");
            EntityManager em = emf.createEntityManager();
     
            Query Kysymykset = em.createNamedQuery("Kysymykset2.findAll");
          
            List<Kysymykset2> kysymysList = Kysymykset.getResultList();
           
            for(int i = 0; i < kysymysList.size(); i++) {       
            int valittuID = kysymysList.get(i).getKysymysId();
            out.println("<a href='http://localhost:8080/vaalikone/Muokkaus.jsp?ID="+ valittuID + "'>Muokkaa</a>" + " ");
            out.println(kysymysList.get(i).getKysymysId() + " | ");
            out.println(kysymysList.get(i).getKysymys() + "<br/>");
            
            
            
            
            
            
                    
            
            
            
            
            
        }

        
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Muokkaus</title>");            
            out.println("</head>");
            out.println("<body>");
//            out.println("<h1>Servlet Muokkaus at " + "" + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
