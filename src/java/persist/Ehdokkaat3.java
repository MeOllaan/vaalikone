/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persist;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author antti1419
 */
@Entity
@Table(name = "EHDOKKAAT3", schema="APP")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ehdokkaat3.findAll", query = "SELECT e FROM Ehdokkaat3 e"),
    @NamedQuery(name = "Ehdokkaat3.findByEhdokasId", query = "SELECT e FROM Ehdokkaat3 e WHERE e.ehdokasId = :ehdokasId"),
    @NamedQuery(name = "Ehdokkaat3.findByKayttajatunnus", query = "SELECT e FROM Ehdokkaat3 e WHERE e.kayttajatunnus = :kayttajatunnus"),
    @NamedQuery(name = "Ehdokkaat3.findByEtunimi", query = "SELECT e FROM Ehdokkaat3 e WHERE e.etunimi = :etunimi"),
    @NamedQuery(name = "Ehdokkaat3.findBySukunimi", query = "SELECT e FROM Ehdokkaat3 e WHERE e.sukunimi = :sukunimi"),
    @NamedQuery(name = "Ehdokkaat3.findByPuolue", query = "SELECT e FROM Ehdokkaat3 e WHERE e.puolue = :puolue"),
    @NamedQuery(name = "Ehdokkaat3.findByKotipaikkakunta", query = "SELECT e FROM Ehdokkaat3 e WHERE e.kotipaikkakunta = :kotipaikkakunta"),
    @NamedQuery(name = "Ehdokkaat3.findByIka", query = "SELECT e FROM Ehdokkaat3 e WHERE e.ika = :ika"),
    @NamedQuery(name = "Ehdokkaat3.findByMiksiEduskuntaan", query = "SELECT e FROM Ehdokkaat3 e WHERE e.miksiEduskuntaan = :miksiEduskuntaan"),
    @NamedQuery(name = "Ehdokkaat3.findByMitaAsioitaHaluatEdistaa", query = "SELECT e FROM Ehdokkaat3 e WHERE e.mitaAsioitaHaluatEdistaa = :mitaAsioitaHaluatEdistaa"),
    @NamedQuery(name = "Ehdokkaat3.findByAmmatti", query = "SELECT e FROM Ehdokkaat3 e WHERE e.ammatti = :ammatti")})
public class Ehdokkaat3 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "EHDOKAS_ID")
    private Integer ehdokasId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "KAYTTAJATUNNUS")
    private String kayttajatunnus;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "ETUNIMI")
    private String etunimi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "SUKUNIMI")
    private String sukunimi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "PUOLUE")
    private String puolue;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "KOTIPAIKKAKUNTA")
    private String kotipaikkakunta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IKA")
    private int ika;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2500)
    @Column(name = "MIKSI_EDUSKUNTAAN")
    private String miksiEduskuntaan;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2500)
    @Column(name = "MITA_ASIOITA_HALUAT_EDISTAA")
    private String mitaAsioitaHaluatEdistaa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "AMMATTI")
    private String ammatti;

    public Ehdokkaat3() {
    }

    public Ehdokkaat3(Integer ehdokasId) {
        this.ehdokasId = ehdokasId;
    }

    public Ehdokkaat3(String kayttajatunnus, String etunimi, String sukunimi, String puolue, String kotipaikkakunta, int ika, String miksiEduskuntaan, String mitaAsioitaHaluatEdistaa, String ammatti) {
        this.ehdokasId = ehdokasId;
        this.kayttajatunnus = kayttajatunnus;
        this.etunimi = etunimi;
        this.sukunimi = sukunimi;
        this.puolue = puolue;
        this.kotipaikkakunta = kotipaikkakunta;
        this.ika = ika;
        this.miksiEduskuntaan = miksiEduskuntaan;
        this.mitaAsioitaHaluatEdistaa = mitaAsioitaHaluatEdistaa;
        this.ammatti = ammatti;
    }

    public Integer getEhdokasId() {
        return ehdokasId;
    }

    public void setEhdokasId(Integer ehdokasId) {
        this.ehdokasId = ehdokasId;
    }

    public String getKayttajatunnus() {
        return kayttajatunnus;
    }

    public void setKayttajatunnus(String kayttajatunnus) {
        this.kayttajatunnus = kayttajatunnus;
    }

    public String getEtunimi() {
        return etunimi;
    }

    public void setEtunimi(String etunimi) {
        this.etunimi = etunimi;
    }

    public String getSukunimi() {
        return sukunimi;
    }

    public void setSukunimi(String sukunimi) {
        this.sukunimi = sukunimi;
    }

    public String getPuolue() {
        return puolue;
    }

    public void setPuolue(String puolue) {
        this.puolue = puolue;
    }

    public String getKotipaikkakunta() {
        return kotipaikkakunta;
    }

    public void setKotipaikkakunta(String kotipaikkakunta) {
        this.kotipaikkakunta = kotipaikkakunta;
    }

    public int getIka() {
        return ika;
    }

    public void setIka(int ika) {
        this.ika = ika;
    }

    public String getMiksiEduskuntaan() {
        return miksiEduskuntaan;
    }

    public void setMiksiEduskuntaan(String miksiEduskuntaan) {
        this.miksiEduskuntaan = miksiEduskuntaan;
    }

    public String getMitaAsioitaHaluatEdistaa() {
        return mitaAsioitaHaluatEdistaa;
    }

    public void setMitaAsioitaHaluatEdistaa(String mitaAsioitaHaluatEdistaa) {
        this.mitaAsioitaHaluatEdistaa = mitaAsioitaHaluatEdistaa;
    }

    public String getAmmatti() {
        return ammatti;
    }

    public void setAmmatti(String ammatti) {
        this.ammatti = ammatti;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ehdokasId != null ? ehdokasId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ehdokkaat3)) {
            return false;
        }
        Ehdokkaat3 other = (Ehdokkaat3) object;
        if ((this.ehdokasId == null && other.ehdokasId != null) || (this.ehdokasId != null && !this.ehdokasId.equals(other.ehdokasId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persist.Ehdokkaat3[ ehdokasId=" + ehdokasId + " ]";
    }
    
}
