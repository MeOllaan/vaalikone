/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persist;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author janina1403
 * 
 * Tämä on Kysymykset2-taulusta generoitu entity-luokka. Samanlainen kuin alkuperäinen Kysymykset-luokka, mutta käyttää auto incrementiä.
 * 
 */
@Entity
@Table(name = "KYSYMYKSET2", schema="APP")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Kysymykset2.findAll", query = "SELECT k FROM Kysymykset2 k"),
    @NamedQuery(name = "Kysymykset2.findByKysymysId", query = "SELECT k FROM Kysymykset2 k WHERE k.kysymysId = :kysymysId"),
    @NamedQuery(name = "Kysymykset2.findByKysymys", query = "SELECT k FROM Kysymykset2 k WHERE k.kysymys = :kysymys")})
public class Kysymykset2 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "KYSYMYS_ID")
    private Integer kysymysId;
    @Size(max = 100)
    @Column(name = "KYSYMYS")
    private String kysymys;

    public Kysymykset2() {
    }

    public Kysymykset2(String kysymysTeksti) {
        this.kysymysId = kysymysId;
        this.kysymys = kysymysTeksti;
    }

    public Integer getKysymysId() {
        return kysymysId;
    }

    public void setKysymysId(Integer kysymysId) {
        this.kysymysId = kysymysId;
    }

    public String getKysymys() {
        return kysymys;
    }

    public void setKysymys(String kysymys) {
        this.kysymys = kysymys;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kysymysId != null ? kysymysId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kysymykset2)) {
            return false;
        }
        Kysymykset2 other = (Kysymykset2) object;
        if ((this.kysymysId == null && other.kysymysId != null) || (this.kysymysId != null && !this.kysymysId.equals(other.kysymysId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persist.Kysymykset2[ kysymysId=" + kysymysId + " ]";
    }
    
}
