/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persist;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author antti1419
 */
@Entity
@Table(name = "TUNNUKSET", schema = "APP")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tunnukset.findAll", query = "SELECT t FROM Tunnukset t"),
    @NamedQuery(name = "Tunnukset.findByKayttajatunnus", query = "SELECT t FROM Tunnukset t WHERE t.kayttajatunnus = :kayttajatunnus"),
    @NamedQuery(name = "Tunnukset.findBySalasana", query = "SELECT t FROM Tunnukset t WHERE t.salasana = :salasana"),
    @NamedQuery(name = "Tunnukset.findByRooli", query = "SELECT t FROM Tunnukset t WHERE t.rooli = :rooli")})
public class Tunnukset implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "KAYTTAJATUNNUS")
    private String kayttajatunnus;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "SALASANA")
    private String salasana;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ROOLI")
    private int rooli;

    public Tunnukset() {
    }

    public Tunnukset(String kayttajatunnus) {
        this.kayttajatunnus = kayttajatunnus;
    }

    public Tunnukset(String kayttajatunnus, String salasana, int rooli) {
        this.kayttajatunnus = kayttajatunnus;
        this.salasana = salasana;
        this.rooli = rooli;
    }

    public String getKayttajatunnus() {
        return kayttajatunnus;
    }

    public void setKayttajatunnus(String kayttajatunnus) {
        this.kayttajatunnus = kayttajatunnus;
    }

    public String getSalasana() {
        return salasana;
    }

    public void setSalasana(String salasana) {
        this.salasana = salasana;
    }

    public int getRooli() {
        return rooli;
    }

    public void setRooli(int rooli) {
        this.rooli = rooli;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kayttajatunnus != null ? kayttajatunnus.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tunnukset)) {
            return false;
        }
        Tunnukset other = (Tunnukset) object;
        if ((this.kayttajatunnus == null && other.kayttajatunnus != null) || (this.kayttajatunnus != null && !this.kayttajatunnus.equals(other.kayttajatunnus))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persist.Tunnukset[ kayttajatunnus=" + kayttajatunnus + " ]";
    }
    
}
