/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KysymystenHallinta;

import java.util.List.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import persist.Kysymykset2;



/**
 *
 * @author juho1211
 */
public class PoistaKysymysServlet extends HttpServlet {

    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        HttpSession session = request.getSession();
        
        if (session.getAttribute("Admin") == null) {
            
        out.println("<h1>Käyttö estetty!!</h1>");
            
        } else {
        
        EntityManagerFactory emf
                = (EntityManagerFactory) getServletContext().getAttribute("emf");
        EntityManager em = emf.createEntityManager();
        
        
        //POISTETAAN KYSYMYS TIETOKANNASTA
        String ID = request.getParameter("ID");
        
       out.println(ID);
       
       if (ID==null){
           out.println("Therewas an error");
       }
       else{
           Kysymykset2 kysymys = em.find(Kysymykset2.class, Integer.parseInt(ID));
           em.getTransaction().begin();
            em.remove(kysymys);
           em.getTransaction().commit();
           
           
           
           out.println("Poistetaan kysymys ID:llä: " + ID);
           Query poistetaanV = em.createNativeQuery("DELETE FROM APP.VASTAUKSET WHERE KYSYMYS_ID="+ID);
           em.getTransaction().begin();
           poistetaanV.executeUpdate();
           em.getTransaction().commit();
           em.close();
           //Ohjataan takaisin jsp sivulle
           request.getRequestDispatcher("/PoistaKysymys.jsp")
                            .forward(request, response);
       }
           
        
          try {
            
            
        } finally {            
            out.close();
        }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
