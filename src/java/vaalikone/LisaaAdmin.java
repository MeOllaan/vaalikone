/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vaalikone;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.persistence.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.servlet.http.HttpSession;
import persist.Tunnukset;






/**
 *
 * @author pekka1407
 */
@WebServlet(name = "LisaaAdmin", urlPatterns = {"/LisaaAdmin"})
public class LisaaAdmin extends HttpServlet {

    // Alustetaan hash
    Hash hash = new Hash();
    
     
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        
        // Tarkastetaan session
        if (session.getAttribute("Admin") == null) {
            
        out.println("<h1>Käyttö estetty!!</h1>");
            
        } else {
        
        // otetaan parametrit vastaan
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String hashpassword = hash.crypt(password);
        int rooli = Integer.parseInt(request.getParameter("rooli"));
        
        
        
        EntityManagerFactory emf = (EntityManagerFactory) getServletContext().getAttribute("emf");
        EntityManager em = emf.createEntityManager();
        
        try {
            /* TODO output your page here. You may use following sample code. */
      
      // Lisätään entity managerilla uusi käyttäjä tietokantaan
       Tunnukset tunnus = new Tunnukset(username, hashpassword, rooli);
         
        em.getTransaction().begin();
        em.persist(tunnus);
        em.getTransaction().commit();
    
        
           em.close();

           // Jos uusi lisätty käyttäjä on ehdokas, siirtyy ohjelma ehdokkaan tietojen täyttämiseen. Muutoin annetaan onnistumisilmoitus.
           if (rooli == 1 ){
                  request.getRequestDispatcher("/AsetaEhdokkaanTiedot.jsp?username=" + username)
                            .forward(request, response);
           }
           else 
          request.getRequestDispatcher("/Onnistui")
                            .forward(request, response);
      
      //  request.getRequestDispatcher("/vaalikone.jsp");
        
  } finally {            
          out.close();
         
        }
    }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
