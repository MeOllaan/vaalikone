/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vaalikone;

import java.awt.Window;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletConfig;
import java.sql.*;
import java.util.List;
import javax.jms.Session;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import persist.Tunnukset;
import javax.servlet.http.HttpSession;

/**
 *
 * @author janina1403
 */
@WebServlet(name = "Kirjautuminen", urlPatterns = {"/kirjautuminen"})
public class Kirjautuminen extends HttpServlet {

    Hash hash = new Hash();

    /**
     * Tämä servlet suorittaa kirjautumisen, eli ottaa parametrit vastaan, tarkistaa että ne varmasti löytyvät tietokannasta, 
     * ja tämän jälkeen luo session sekä siirtää käyttäjän oikealle roolinsamukaiselle sivustolle. 
  
     */
    //String username = request.getParameter("username");
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        // Otetaan parametrit vastaan
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String hashpassword = hash.crypt(password);

        // Entitymanagerin avulla haetaan kaikki käyttäjätiedot tietokannasta, ja muodostetaan lista. Tämän jälkeen lista käydään lävitse, ja jos 
        // löydetään annettut tiedot, muodostetaan sessio ja käyttäjä lasketaan eteenpäin
        
        EntityManagerFactory emf = (EntityManagerFactory) getServletContext().getAttribute("emf");
        EntityManager em = emf.createEntityManager();

        Query kaikkikysymykset = em.createNamedQuery("Tunnukset.findAll");

        List<Tunnukset> kysymysList = kaikkikysymykset.getResultList();

       // Listan läpikäynti ja tarkistus try-catchin sisällä
        try {
            for (int i = 0; i < kysymysList.size(); i++) {


                if (kysymysList.get(i).getKayttajatunnus().equals(username) && kysymysList.get(i).getSalasana().equals(hashpassword)) {



                       // Tietokannasta etsitään tieto onko rooli ehdokas vai admin. Tästä riippuen käyttäjä siirretään oikealle sivustolle. 

                    if (kysymysList.get(i).getRooli() == 1) {
                        //asetetaan sessionit Ehdokkaalle
                        HttpSession session = request.getSession(true);
                        session.setAttribute("Ehdokas", username);
                        session.setMaxInactiveInterval(60 * 30);
                        request.getRequestDispatcher("/ehdokasPaneeli.jsp")
                                .forward(request, response);
                    } else if (kysymysList.get(i).getRooli() == 2) {
                        //asetetaan sessionit Admineille
                        HttpSession session = request.getSession(true);
                        session.setAttribute("Admin", username);
                        session.setMaxInactiveInterval(60 * 30);
                        request.getRequestDispatcher("/AdminTools.jsp")
                                .forward(request, response);
                    } else {
                        request.getRequestDispatcher("/vaalikone")
                                .forward(request, response);
                    }

                }



            }
        } finally {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            //  emf.close();
        }




        try {

            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Kirjautuminen</title>");
            out.println("<link href=\"style.css\" rel=\"stylesheet\" type=\"text/css\">");
            out.println("</head>");
            out.println("<body>");
            out.println("<img id=" + "headerimg" + " src=" + "images/Logo.png" + " width= " + "500" + " height=" + "144" + "/>");
            out.println("<div id=" + "container" + ">");
            // out.println("<h1>Servlet Kirjautuminen at " + getUserId(username)+  "</h1>");
            out.println("\n Käyttäjänimi tai salasa on väärin! ");
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
