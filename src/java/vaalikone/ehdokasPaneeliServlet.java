/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vaalikone;

import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import persist.Ehdokkaat3;

/**
 *
 * @author janina1403
 */
public class ehdokasPaneeliServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        //Haetaan parametrinä saatu ehdokkaan id ja vastaukset kysymyksiin 
        int ehdokasID = Integer.parseInt(request.getParameter("ehdokasid"));
        String miksiEduskuntaan = request.getParameter("miksieduskuntaan");
        String edistys = request.getParameter("edistys");
        
        // Ludoaan uusi entitymanager
        EntityManagerFactory emf = (EntityManagerFactory) getServletContext().getAttribute("emf");
        EntityManager em = emf.createEntityManager();
        
        try {
            // Haetaan ehdokas tietokannasta ehdokas ID:n perusteella
            Ehdokkaat3 ehdokas = em.find(Ehdokkaat3.class, ehdokasID);
            
            //Muokataan vastauksia
            em.getTransaction().begin();
            ehdokas.setMiksiEduskuntaan(miksiEduskuntaan);
            ehdokas.setMitaAsioitaHaluatEdistaa(edistys);
            em.getTransaction().commit();
            
            
        } finally {
            // suljetaan entitymanager ja ohjataan takaisin ehdokaspaneelille
            em.close();
            response.sendRedirect("/vaalikone/ehdokasPaneeli.jsp");

        }



    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
