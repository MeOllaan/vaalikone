/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vaalikone;

import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import persist.Ehdokkaat3;





/**
 *
 * @author pekka1407
 */
public class EhdokasTiedot extends HttpServlet {

    /**
     * Tässä servletissä laitetaan tietokantaan loput annettut Ehdokkaan tiedot. 
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        // Parametrien vastaanotto
        
        String kayttajatunnus = request.getParameter("username");
        String etunimi = request.getParameter("etunimi");
        String sukunimi = request.getParameter("sukunimi");
        String puolue = request.getParameter("puolue");
        String kotikunta = request.getParameter("kotikunta");
        int ika = Integer.parseInt(request.getParameter("ika"));
        String ammatti = request.getParameter("ammatti");
        String tsek = kayttajatunnus+etunimi+sukunimi+puolue+kotikunta+ammatti+ika;    
        
    
           EntityManagerFactory emf
                = (EntityManagerFactory) getServletContext().getAttribute("emf");
        EntityManager em = emf.createEntityManager();
        
        // Muualla toteutuksessa ehdokas saa itse täyttää muutaman laajemman kohdan, joten annetaan niille aluksi vain placeholderit
        String a = "Ehdokas täyttää";
        String c = "Ehdokas Täyttää";
      
        
        try {
            /* Laitetaan tiedot tietokantaan ja siirrytään takaisin AdminToolsiin*/
           
        Ehdokkaat3 ehdokas = new Ehdokkaat3(kayttajatunnus, etunimi, sukunimi,puolue,kotikunta,ika,a,c,ammatti);
        em.getTransaction().begin();
        em.persist(ehdokas);
        em.getTransaction().commit();
    
         request.getRequestDispatcher("/AdminTools.jsp")
                            .forward(request, response);
            
              
       
        } finally {            
            out.close();
              em.close();
               
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
