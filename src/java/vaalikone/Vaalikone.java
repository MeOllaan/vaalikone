/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vaalikone;

import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import persist.Ehdokkaat3;
import persist.Kysymykset2;
import persist.Vastaukset;

/**
 * Vaalikone-servlet, vastaa vaalikoneen varsinaisesta toiminnallisuudesta
 *
 * @author Jonne
 */
public class Vaalikone extends HttpServlet {

    //hae java logger-instanssi
    private final static Logger logger = Logger.getLogger(Loki.class.getName());

    //VOI PERKELE
    //KysymystenMaara on periaatteessa laskuri, jonka avulla haetaan aina seuraavan kysymyksen ID listasta.
    //Kysymys_id:tä ei voi käyttää seuraavan kysymyksen hakemiseen,
    //koska Kysymykset2-taulun kysymys_ID toteutetaan auto incrementiä käyttäen.
    public int kysymystenMaara = 0;

    public int getKysymystenMaara() {
        return kysymystenMaara;
    }

    public void setKysymystenMaara(int kysymystenMaara) {
        this.kysymystenMaara = kysymystenMaara;
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int kysymys_id;

        // hae http-sessio ja luo uusi jos vanhaa ei ole vielä olemassa
        HttpSession session = request.getSession(true);

        //hae käyttäjä-olio http-sessiosta
        Kayttaja usr = (Kayttaja) session.getAttribute("usrobj");

        //jos käyttäjä-oliota ei löydy sessiosta, luodaan sinne sellainen
        if (usr == null) {
            usr = new Kayttaja();
            logger.log(Level.FINE, "Luotu uusi käyttäjä-olio");
            session.setAttribute("usrobj", usr);
        }

        // Hae tietokanta-yhteys contextista
        EntityManagerFactory emf = (EntityManagerFactory) getServletContext().getAttribute("emf");
        EntityManager em = emf.createEntityManager();

        //Haetaan kaikki kysymykset tietokannasta ja tehdään niistä lista.
        Query haeKaikki = em.createNamedQuery("Kysymykset2.findAll");
        List<Kysymykset2> kaikki = haeKaikki.getResultList();

        //hae url-parametri func joka määrittää toiminnon mitä halutaan tehdä.
        //func=haeEhdokas: hae tietyn ehdokkaan tiedot ja vertaile niitä käyttäjän vastauksiin
        //Jos ei määritelty, esitetään kysymyksiä.
        String strFunc = request.getParameter("func");

        if (strFunc == null) {

            //hae parametrinä tuotu edellisen kysymyksen nro
            String strKysymys_id = request.getParameter("q");

            //hae parametrina tuotu edellisen kysymyksen vastaus
            String strVastaus = request.getParameter("vastaus");

            // Jos kysymyksen numero (kysId) on asetettu, haetaan tuo kysymys
            // muuten haetaan kysnro 1
            if (strKysymys_id == null) {

                //asetetaan kysymystenMaara aluksi nollaksi, koska riveittäin ensimmäisen kysymyksen numero on 0.
                kysymystenMaara = 0;
                kysymys_id = kaikki.get(kysymystenMaara).getKysymysId();

            } else {

                kysymys_id = parseInt(strKysymys_id);

                //jos vastaus on asetettu, tallenna se session käyttäjä-olioon.
                if (strVastaus != null) {
                    usr.addVastaus(kysymystenMaara, parseInt(strVastaus));
                }

                //määritä seuraavaksi haettava kysymys
                kysymystenMaara++;

                //Jos kysymyksiä on vielä listassa jäljellä haetaan seuraavan rivin kysymyksen ID.
                if (kysymystenMaara < kaikki.size()) {
                    kysymys_id = kaikki.get(kysymystenMaara).getKysymysId();
                }

            }

            //jos kysymyksiä on vielä jäljellä, hae seuraava
            if (kysymystenMaara < kaikki.size()) {
                try {
                    //Hae haluttu kysymys tietokannasta
                    Query q = em.createQuery(
                            "SELECT k FROM Kysymykset2 k WHERE k.kysymysId=?1");
                    q.setParameter(1, kysymys_id);
                    //Lue haluttu kysymys listaan
                    List<Kysymykset2> kysymysList = q.getResultList();
                    request.setAttribute("kysymykset", kysymysList);
                    request.getRequestDispatcher("/vastaus.jsp")
                            .forward(request, response);

                } finally {
                    // Sulje tietokantayhteys
                    if (em.getTransaction().isActive()) {
                        em.getTransaction().rollback();
                    }
                    em.close();
                }

                //jos kysymykset loppuvat, lasketaan tulos!
            } else {

                //Tyhjennetään piste-array jotta pisteet eivät tuplaannu mahdollisen refreshin tapahtuessa
                for (int i = 0; i < kaikki.size(); i++) {
                    usr.pisteet.set(i, new Tuple<>(0, 0));
                }

                //Hae lista ehdokkaista
                Query qE = em.createNamedQuery("Ehdokkaat3.findAll");
                List<Ehdokkaat3> ehdokasList = qE.getResultList();
                
                //iteroi ehdokaslista läpi
                for (int i = 1; i < ehdokasList.size(); i++) {
                    
                    //Hae lista ehdokkaiden vastauksista
                    Query qV = em.createQuery(
                            "SELECT v FROM Vastaukset v WHERE v.vastauksetPK.ehdokasId=?1");
                    qV.setParameter(1, ehdokasList.get(i).getEhdokasId());
                    List<Vastaukset> vastausList = qV.getResultList();

                    //iteroi vastauslista läpi
                    for (Vastaukset eVastaus : vastausList) {
                        int pisteet;

                        //hae käyttäjän ehdokaskohtaiset pisteet
                        pisteet = usr.getPisteet(i);

                        //laske oman ja ehdokkaan vastauksen perusteella pisteet 
                        pisteet += laskePisteet(usr.getVastaus(i), eVastaus.getVastaus());

                        logger.log(Level.INFO, "eID: {0} / k: {1} / kV: {2} / eV: {3} / p: {4}", new Object[]{i, eVastaus.getVastauksetPK().getKysymysId(), usr.getVastaus(i), eVastaus.getVastaus(), pisteet});
                        usr.addPisteet(i, pisteet);
                    }

                }

                //siirrytään hakemaan paras ehdokas
                strFunc = "haeEhdokas";
            }

        }

        //jos func-arvo on haeEhdokas, haetaan haluttu henkilö käyttäjälle sopivimmista ehdokkaista
        if ("haeEhdokas".equals(strFunc)) {
            //luetaan url-parametristä "top-listan järjestysnumero". Jos ei määritelty, haetaan PARAS vaihtoehto.
            String strJarjestysnumero = request.getParameter("numero");
            Integer jarjestysnumero = 0;
            if (strJarjestysnumero != null) {
                jarjestysnumero = Integer.parseInt(strJarjestysnumero);
            }

            //Lue käyttäjälle sopivimmat ehdokkaat väliaikaiseen Tuple-listaan.
            List<Tuple<Integer, Integer>> tpl = usr.haeParhaatEhdokkaat();

            //hae määritetyn ehdokkaan tiedot
            Query q = em.createQuery(
                    "SELECT e FROM Ehdokkaat3 e WHERE e.ehdokasId=?1");
            q.setParameter(1, tpl.get(jarjestysnumero).ehdokasId);
            List<Ehdokkaat3> parasEhdokas = q.getResultList();

            //hae ko. ehdokkaan vastaukset
            q = em.createQuery(
                    "SELECT v FROM Vastaukset v WHERE v.vastauksetPK.ehdokasId=?1");
            q.setParameter(1, tpl.get(jarjestysnumero).ehdokasId);
            List<Vastaukset> parhaanEhdokkaanVastaukset = q.getResultList();

            //hae kaikki kysymykset
            q = em.createQuery(
                    "SELECT k FROM Kysymykset2 k");
            List<Kysymykset2> kaikkiKysymykset = q.getResultList();

            //ohjaa tiedot tulosten esityssivulle
            request.setAttribute("kaikkiKysymykset", kaikkiKysymykset);
            request.setAttribute("kayttajanVastaukset", usr.getVastausLista());
            request.setAttribute("parhaanEhdokkaanVastaukset", parhaanEhdokkaanVastaukset);
            request.setAttribute("parasEhdokas", parasEhdokas);
            request.setAttribute("pisteet", tpl.get(jarjestysnumero).pisteet);
            request.setAttribute("jarjestysnumero", jarjestysnumero);
            request.getRequestDispatcher("/tulokset.jsp")
                    .forward(request, response);

            // Sulje tietokantayhteys
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            em.close();

        }

    }

    private Integer laskePisteet(Integer kVastaus, Integer eVastaus) {
        int pisteet = 0;
        if (kVastaus - eVastaus == 0) {
            pisteet = 3;
        }
        if (kVastaus - eVastaus == 1 || kVastaus - eVastaus == -1) {
            pisteet = 2;
        }
        if (kVastaus - eVastaus == 2 || kVastaus - eVastaus == -2 || kVastaus - eVastaus == 3 || kVastaus - eVastaus == -3) {
            pisteet = 1;
        }

        //if (kVastaus - eVastaus == 4 || kVastaus - eVastaus == -4) pisteet = 0;
        return pisteet;

    }

    //<editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
