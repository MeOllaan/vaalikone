/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vaalikone;

import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Integer.parseInt;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import persist.Kysymykset2;
import persist.Kysymykset;
import persist.Vastaukset;
import persist.VastauksetPK;
import vaalikone.Session;

/**
 *
 * @author janina1403
 */
public class ehdokkaanVastausServlet extends HttpServlet {

    public int kysymystenMaara = 0;

    public int getKysymystenMaara() {
        return kysymystenMaara;
    }

    public void setKysymystenMaara(int kysymystenMaara) {
        this.kysymystenMaara = kysymystenMaara;
    }

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        int kysymys_id;
        HttpSession session = request.getSession(true);
        
        
        String kysymysTeksti;

        EntityManagerFactory emf = (EntityManagerFactory) getServletContext().getAttribute("emf");
        EntityManager em = emf.createEntityManager();

        int ehdokas_id = 3;
        
        
        //Haetaan kaikki kysymykset tietokannasta ja tehdään niistä lista.
        Query haeKaikki = em.createNamedQuery("Kysymykset2.findAll");
        List<Kysymykset2> kaikki = haeKaikki.getResultList();

        //hae parametrinä tuotu edellisen kysymyksen nro
        String strKysymys_id = request.getParameter("q");

        //hae parametrina tuotu edellisen kysymyksen vastaus
        String strVastaus = request.getParameter("vastaus");

        String kommentti = request.getParameter("kommentti");



        if (strKysymys_id == null) {

            //asetetaan kysymystenMaara aluksi nollaksi, koska riveittäin ensimmäisen kysymyksen numero on 0.
            kysymystenMaara = 0;
            kysymys_id = kaikki.get(kysymystenMaara).getKysymysId();


        } else {

            kysymys_id = Integer.parseInt(strKysymys_id);

            if (strVastaus != null) {

                em.getTransaction().begin();


                Vastaukset vastaus = new Vastaukset(ehdokas_id, kysymys_id);

                vastaus.setVastaus(Integer.parseInt(strVastaus));
                vastaus.setKommentti(kommentti);
                
                em.merge(vastaus);

                em.getTransaction().commit();

            }

            kysymystenMaara++;

            if (kysymystenMaara < kaikki.size()) {
                kysymys_id = kaikki.get(kysymystenMaara).getKysymysId();
            }


        }

        if (kysymystenMaara < kaikki.size()) {

            try {
                em.getTransaction().begin();
                Kysymykset2 kysymys = em.find(Kysymykset2.class, kysymys_id);


                kysymysTeksti = kysymys.getKysymys();
                em.getTransaction().commit();

                request.setAttribute("kysymys", kysymysTeksti);
                request.setAttribute("kysymysID", kysymys_id);



                request.getRequestDispatcher("/ehdokkaanVastaus.jsp")
                        .forward(request, response);

            } finally {
            }

        } else {

            response.sendRedirect("/vaalikone/ehdokasPaneeli.jsp");



        }




    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
