/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vastTallennus;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import persist.Ehdokkaat3;
import persist.Vastaukset;
import persist.VastauksetPK;

/**
 *
 * @author juho1211
 */
public class VastaustenTallennusServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession(true);
        
        if (session.getAttribute("Ehdokas") == null) {

            out.println("Käyttö estetty!!");
           

        } else {
        
        EntityManagerFactory emf = (EntityManagerFactory) getServletContext().getAttribute("emf");
        EntityManager em = emf.createEntityManager();




        String kayttajatunnus = session.getAttribute("Ehdokas").toString();
        Query haeID = em.createNativeQuery("select EHDOKAS_ID from APP.EHDOKKAAT3 WHERE KAYTTAJATUNNUS = " + "'" + kayttajatunnus + "'");




        em.getTransaction().begin();

        int ehdokasID = Integer.parseInt(haeID.getResultList().get(0).toString());
        em.getTransaction().commit();

        Map<String, String[]> parameters = request.getParameterMap();






        Set set = parameters.entrySet(); //nro on HashMap hajautustaulun nimi.
        Iterator iterator = set.iterator();

        while (iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry) iterator.next();

            if (!mentry.getKey().toString().startsWith("k")) {

                int id = Integer.parseInt(mentry.getKey().toString());
                int v = Integer.parseInt(request.getParameter(mentry.getKey().toString()));
                String k = request.getParameter("k" + id);

                em.getTransaction().begin();

                Vastaukset vastaus = new Vastaukset(ehdokasID, id);
                vastaus.setVastaus(v);
                vastaus.setKommentti(k);
                em.merge(vastaus);

                em.getTransaction().commit();

            }
        }

        


        request.getRequestDispatcher("/ehdokasPaneeli.jsp")
                .forward(request, response);
        try {
        } finally {
            em.close();
            out.close();

        }
        }
    }

    public void createVastaus(int id, int v, String k) {
        VastauksetPK vastauspk = new VastauksetPK();
        Vastaukset vastaus = new Vastaukset();
        vastauspk.setEhdokasId(2);
        vastauspk.setKysymysId(id);
        vastaus.setVastaus(v);
        vastaus.setKommentti(k);

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
