<%-- 
    Document   : LisaaAdmin
    Created on : May 6, 2016, 1:51:47 PM
    Author     : pekka1407
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link href="style.css" rel="stylesheet" type="text/css">
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Me Ollaan Vaalikone 2.0 Lisää admin tai ehdokas</title>

        <script>
            function alphanumeric()
            {
                var letters = /^[a-zA-Z]+$/;
                var virheLaskuri = 0;
                var rooli = document.forms["login"]["rooli"].value;
                var kayttaja = document.forms["login"]["username"].value;
                var salasana = document.forms["login"]["password"].value;
                var kaikki = kayttaja + salasana;


                if (!kaikki.match(letters))
                {
                    virheLaskuri++;
                }


                if (virheLaskuri > 0)
                {
                    alert('Vain kirjaimia! Rooliin vain 1 tai 2!');
                    return false;

                } else {

                    return true;
                }

            }
        </script>
    </head>
    <body>
        <img id="headerimg" src="images/Logo.png" width="500" height="144" alt=""/>
        <div id="container">

            <% if (session.getAttribute("Admin") == null) { %>

            <h1>Käyttö estetty!!</h1>


            <% } else { %>

            <form action="AdminTools.jsp" method="post">
                <input type="submit" value="Takaisin" />
            </form>

            <form name="login" action="LisaaAdmin" method="post" onsubmit="return alphanumeric()">
                <center>Käyttäjätunnus:</center>
                <center><input name="username" size="14" /></center>
                <center>Salasana:</center>
                <center><input name="password" type="password" size="14" /></center>
                <center>ROOLI (1 = Ehdokas, 2 = Admin)</center>
                <center><input name="rooli" size="14" /></center>
                <br>
                <center><input type="submit" name="submit" value="Luo tunnukset" /></center>
            </form>
            <% }%>
        </div>
    </body>
</html>
